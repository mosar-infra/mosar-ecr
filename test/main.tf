# ws ecr /main.tf

module "ecr" {
  source       = "git::https://gitlab.com/mosar-infra/tf-module-ecr.git?ref=tags/v1.1.2"
  repositories = local.repositories
  environment  = var.environment
  managed_by   = var.managed_by
}

