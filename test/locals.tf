# ws ecr /locals.tf

locals {
  repositories = {
    flagr = {
      name                 = "flagr"
      image_tag_mutability = "MUTABLE"
      scan_on_push         = true
      lifecycle_policy     = file("./lifecycle_policies/lifecycle_policy.json")
    }
    jenkins-master = {
      name                 = "jenkins-master"
      image_tag_mutability = "MUTABLE"
      scan_on_push         = true
      lifecycle_policy     = file("./lifecycle_policies/jenkins_lifecycle_policy.json")
    }
    kaniko = {
      name                 = "kaniko"
      image_tag_mutability = "MUTABLE"
      scan_on_push         = true
      lifecycle_policy     = file("./lifecycle_policies/lifecycle_policy.json")
    }
    mosar-frontend = {
      name                 = "mosar-frontend"
      image_tag_mutability = "MUTABLE"
      scan_on_push         = true
      lifecycle_policy     = file("./lifecycle_policies/lifecycle_policy.json")
    }
    mosar-flashcard = {
      name                 = "mosar-flashcard"
      image_tag_mutability = "MUTABLE"
      scan_on_push         = true
      lifecycle_policy     = file("./lifecycle_policies/lifecycle_policy.json")
    }
    mosar-game = {
      name                 = "mosar-game"
      image_tag_mutability = "MUTABLE"
      scan_on_push         = true
      lifecycle_policy     = file("./lifecycle_policies/lifecycle_policy.json")
    }
    jenkins-build-agent-backend = {
      name                 = "jenkins-build-agent-backend"
      image_tag_mutability = "MUTABLE"
      scan_on_push         = true
      lifecycle_policy     = file("./lifecycle_policies/lifecycle_policy.json")
    }
    jenkins-build-agent-frontend = {
      name                 = "jenkins-build-agent-frontend"
      image_tag_mutability = "MUTABLE"
      scan_on_push         = true
      lifecycle_policy     = file("./lifecycle_policies/lifecycle_policy.json")
    }
    jenkins-deploy-agent = {
      name                 = "jenkins-deploy-agent"
      image_tag_mutability = "MUTABLE"
      scan_on_push         = true
      lifecycle_policy     = file("./lifecycle_policies/lifecycle_policy.json")
    }
  }
}

